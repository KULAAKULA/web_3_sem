import axios from "axios";

export const client = axios.create({
  baseURL: "http://89.111.136.151:3001/",
  headers: { "Content-Type": "application/json" },
});
