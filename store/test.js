import { makeAutoObservable, toJS } from "mobx";
import { client } from "../axios";

class Test {
    tests = [];
    amount = 0;

    constructor() {
        makeAutoObservable(this);
    }


    getAmount() {
        return this.amount;
    }

    async fetchAmount() {
        const response = await client.get("answersAmount");
        if (response.status == 200) {
            this.amount = response.data?.amount;
        }
    }

    async setAmount(am) {
        const response = await client.post("answersAmount", { amount: am+=1 });
        if (response.status == 200) {
            this.amount = am;
        }
    }

    setTests(data) {
        this.tests = data;
    }

    getTests() {
        return this.tests;
    }

    async fetchTests(setLoading) {
        const response = await client.get("tests");
        if (response.status == 200) {
            this.tests = response.data;
            setTimeout(() => {
                setLoading(false);
            }, 1000);
        }
    }

    async fetchTest(id) {
        const response = await client.get(`tests/${id}`);
        this.setTests(Array(response.data));
    }

    getTest(id) {
        return toJS(this.tests).filter((test) => test?.id == id)[0];
    }
}

export default new Test();
