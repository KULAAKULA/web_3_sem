import { makeAutoObservable } from "mobx";

class User {
  loggedIn = false;

  constructor() {
    makeAutoObservable(this);
  }

  async setLoggedIn(val) {
    this.loggedIn = val;
  }

  getLoggedIn() {
    return this.loggedIn;
  }
}

export default new User();
