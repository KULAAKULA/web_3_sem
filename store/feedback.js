import { makeAutoObservable } from "mobx";
import { client } from "../axios";

class Feedback {
    feedbacks = [];

    constructor() {
        makeAutoObservable(this);
    }

    setFeedback(feedbacks) {
        this.feedbacks = feedbacks;
    }

    getFeedbacks() {
        return this.feedbacks;
    }

    async fetchFeedbacks(setLoading) {
        const response = await client.get("feedback");
        if (response.status == 200) {
            this.setFeedback(response.data);
            setTimeout(() => {
                setLoading(false);
            }, 1000);
        }
    }

    async sendFeedbacks(feedback) {
        const response = await client.post("feedback", feedback);
        if (response.status == 200) {
            this.feedbacks = feedbacks
            setTimeout(() => {
                setLoading(false);
            }, 1000);
        }
    }
}

export default new Feedback();
