import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Test from "../../store/test";
import { toJS } from "mobx";
import { observer } from "mobx-react-lite";
import {
  Button,
  CircularProgress,
  Typography,
  Snackbar,
  Alert,
  Container,
} from "@mui/material";
import Question from "../../components/question";
import Image from "next/image";
import Router from "next/router";
import Navbar from "../../components/navbar";
import Footer from "../../components/footer";
import Head from "next/head";

const DetailPage = observer(() => {
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [answers, setAnswers] = useState({});
  const [errVisibility, setErrVisibility] = useState("none");
  const [isBtnBlocked, setIsBtnBlocked] = useState(false);
  const { id } = router.query;

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    if (id != undefined) {
      if (toJS(Test?.getTest(id)) == undefined) {
        async function fetch() {
          await Test?.fetchTest(id);
        }
        fetch();
      }
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    }
    console.log(process.env.BACK_URL);
  }, [id]);

  const changeAns = (answer) => {
    setAnswers({ ...answers, ...answer });
  };

  const sendAns = async () => {
    if (Object.keys(answers).length < Test?.getTest(id)?.questions?.length) {
      setErrVisibility("block");
    } else {
      setErrVisibility("none");
      setIsBtnBlocked(true);
      await Test?.fetchAmount();
      let am = Test?.amount;
      await Test?.setAmount(am);
      setIsBtnBlocked(false);
      setOpen(true);
      setTimeout(() => {
        Router.push("/");
      }, 2000);
    }
  };

  return (
    <React.Fragment>
      <Head>
        <title>Система оценивания 360 градусов</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Container sx={{ py: 8 }} maxWidth="md">
        {loading ? (
          <CircularProgress />
        ) : (
          <>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
              <Alert
                onClose={handleClose}
                severity="success"
                sx={{ width: "100%" }}
              >
                Ваш ответ успешно отправлен
              </Alert>
            </Snackbar>
            <Typography variant="h1" gutterBottom>
              {Test?.getTest(id)?.name}
            </Typography>
            <Image
              src={Test?.getTest(id)?.image}
              width={150}
              height={150}
              style={{ marginBottom: 20 }}
            />
            <Typography
              variant="body1"
              gutterBottom
              style={{ marginBottom: 20 }}
            >
              {Test?.getTest(id)?.description}
            </Typography>
            {Test?.getTest(id)?.questions?.map((question) => {
              return (
                <Question
                  question={question}
                  key={question?.id}
                  changeAns={changeAns}
                />
              );
            })}
            <Typography color="red" style={{ display: errVisibility }}>
              Вы должны ответить на все вопросы
            </Typography>
            <Button
              variant="contained"
              sx={{ displayPrint: "none" }}
              onClick={sendAns}
              disabled={isBtnBlocked}
            >
              Отправить
            </Button>
          </>
        )}
      </Container>
      <Footer />
    </React.Fragment>
  );
});

export default DetailPage;
