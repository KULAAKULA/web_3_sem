import Head from "next/head";
import Navbar from "../components/navbar";
import {
  Container,
  Grid,
  Typography,
  List,
  ListItem,
  ListItemText,
  Button,
} from "@mui/material";
import { observer } from "mobx-react-lite";
import { toJS } from "mobx";
import React from "react";
import Test from "../store/test";
import DataTable from "../components/table";
import Feedback from "../store/feedback";
import Footer from "../components/footer";
import FeedbackForm from "../components/feedback";

const Home = observer(() => {
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    updateData();
  }, []);

  const updateData = () => {
    setLoading(true);
    Test.fetchTests(setLoading);
    Test.fetchAmount(setLoading);
    Feedback.fetchFeedbacks(setLoading);
  };

  return (
    <>
      <Head>
        <title>Система оценивания 360 градусов</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Navbar />
        <Container sx={{ py: 8 }} maxWidth="md">
          <Typography variant="h2" textAlign="center" marginBottom="40px">
            Тесты системы оценивания "360 градусов"
          </Typography>
          <Typography
            variant="h6"
            textAlign="center"
            marginBottom="40px"
            gutterBottom
          >
            Выберите тест для прохождения
          </Typography>
          <Grid container spacing={4}>
            <DataTable rows={toJS(Test.getTests())} loading={loading} />
          </Grid>
          <Container style={{ alignItems: "flex-start" }}>
            <Typography variant="body1" marginTop="20px" gutterBottom>
              Нажмите на кнопку для обновления списка тестов
            </Typography>
            <Button
              variant="contained"
              style={{ marginBottom: "20px" }}
              onClick={updateData}
            >
              Обновить данные
            </Button>
          </Container>
          <Typography variant="h6" marginTop="20px" gutterBottom>
            Количество пройденных всеми тестов - {toJS(Test.getAmount())}
          </Typography>
          <Typography variant="h6" marginTop="20px" gutterBottom>
            Отзывы
          </Typography>
          <FeedbackForm />
          <List
            sx={{
              width: "100%",
              maxWidth: 360,
              bgcolor: "background.paper",
            }}
          >
            {Feedback?.getFeedbacks()?.map((fback) => (
              <React.Fragment key={fback?.id}>
                <ListItem alignItems="flex-start">
                  <ListItemText
                    primary={fback?.name}
                    secondary={<React.Fragment>{fback?.text}</React.Fragment>}
                  />
                </ListItem>
              </React.Fragment>
            ))}
          </List>
        </Container>
        <Footer />
      </main>
    </>
  );
});

export default Home;
