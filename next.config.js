/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["sun9-54.userapi.com", "sun6-23.userapi.com"],
  },
};

module.exports = nextConfig;
