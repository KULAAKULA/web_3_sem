import { DataGrid } from "@mui/x-data-grid";
import { LinearProgress } from "@mui/material";
import Router from "next/router";

const columns = [
    { field: "id", headerName: "ID", width: 70 },
    {
        field: "name",
        headerName: "Name",
        width: 150,
    },
    {
        field: "description",
        headerName: "Description",
        sortable: false,
        width: 400,
    },
];

export default function DataTable({ rows, loading }) {
    const redirect = (params, event, details) => {
        Router.push(`tests/${params.id}`);
    };

    return (
        <div style={{ height: 500, width: "100%" }}>
            <DataGrid
                loading={loading}
                rows={rows}
                columns={columns}
                onRowClick={redirect}
                pageSize={7}
                rowsPerPageOptions={[7]}
                components={{
                    LoadingOverlay: LinearProgress,
                }}
            />
        </div>
    );
}
