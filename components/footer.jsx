import React from 'react'
import {Box, Container, Typography} from '@mui/material'

const Footer = ()=> {
    return (
        <Box
        component="footer"
        style={{marginTop: 20}}
        sx={{
          py: 3,
          px: 2,
          mt: 'auto',
          backgroundColor: (theme) =>
            theme.palette.mode === 'light'
              ? theme.palette.grey[200]
              : theme.palette.grey[800],
        }}
      >
        <Container maxWidth="sm">
          <Typography variant="body1" textAlign="center">
            Кулакова Е. М. 211-323
          </Typography>
          <Typography variant="body1" textAlign="center">
            <a target="_blank" href='https://gitlab.com/KULAAKULA/web_3_sem'>GIT</a>
          </Typography>
          <Typography variant="body1" textAlign="center">
            <a target="_blank" href='http://89.111.136.151:3001/'>API</a>
          </Typography>
        </Container>
      </Box>
    )
}

export default Footer