import React from "react";
import {
    FormControl,
    FormLabel,
    RadioGroup,
    FormControlLabel,
    Radio,
} from "@mui/material";

const Question = ({ question, changeAns }) => {
    const [ans, setAns] = React.useState(0);
    return (
        <>
            <FormControl required>
                <FormLabel id="demo-radio-buttons-group-label">
                    {question?.question}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"

                >
                    {question?.possibleAnswers?.map((answer) => (
                        <FormControlLabel
                            key={answer?.id}
                            value={answer?.id}
                            control={<Radio />}
                            label={answer?.answer}
                            onChange={(e) => {
                                console.log(e.target.value);
                                changeAns({ [question?.id]: e.target.value });
                            }}
                        />
                    ))}
                </RadioGroup>
            </FormControl>
            <br />
        </>
    );
};

export default Question;
