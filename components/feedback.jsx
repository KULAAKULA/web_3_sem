import React from 'react'
import {
    Button,
    Box,
    TextField,
    CircularProgress,
  } from "@mui/material";
import Feedback from '../store/feedback';


const FeedbackForm = () => {
    const [sendBtnDisabled, setIsSendBtnDisabled] = React.useState(false);
    const [nameErr, setNameErr] = React.useState(false);
    const [textErr, setTextErr] = React.useState(false);
    const [name, setName] = React.useState("");
    const [text, setText] = React.useState("");
    const [loading, setLoading] = React.useState(true)

    const addFeedback = async () => {
        if (name == "") {
          setNameErr(true);
          setTextErr(false);
    
          return;
        }
        if (text == "") {
          setNameErr(false);
          setTextErr(true);
          return;
        }
        setNameErr(false);
        setTextErr(false);
        setIsSendBtnDisabled(true);
        await Feedback.fetchFeedbacks(setLoading);
        let data = Feedback.feedbacks;
        let len = (data.length += 1);
        data.push({ id: len, name, text });
        await Feedback.sendFeedbacks({ id: len, name, text });
        setName("");
        setText("");
        setTimeout(() => {
          setIsSendBtnDisabled(false);
        }, 1000);
      };

    return (
        <Box component="form" sx={{ mt: 1 }} inputMode="text">

            <TextField
              margin="normal"
              required
              fullWidth
              id="name"
              label="Имя"
              name="name"
              value={name}
              error={nameErr}
              onChange={(e) => setName(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="text"
              label="Отзыв"
              type="text"
              id="text"
              value={text}
              error={textErr}
              onChange={(e) => setText(e.target.value)}
            />
            <Button
              onClick={addFeedback}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={sendBtnDisabled}
            >
              Оставить отзыв
            </Button>
        </Box>
    )
}

export default FeedbackForm